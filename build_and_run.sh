#!/bin/bash
set -e
pushd dtupay-core
./build.sh
popd
pushd messaging-utilities
./build.sh
popd