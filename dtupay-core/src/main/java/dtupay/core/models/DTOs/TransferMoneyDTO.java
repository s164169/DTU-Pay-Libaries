package dtupay.core.models.DTOs;

import dtupay.core.DAL.models.Token;

import java.math.BigDecimal;
//@author Magnus Glasdam Jakobsen
public class TransferMoneyDTO {

    public String tokenID;
    public int tokenStatus;
    public String toAccount;
    public String fromAccount;
    public BigDecimal amount;
    public String description;
/*
    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
*/
    public String getTokenID() {
        return tokenID;
    }

    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }

    public int getTokenStatus() {
        return tokenStatus;
    }

    public void setTokenStatus(int tokenStatus) {
        this.tokenStatus = tokenStatus;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
