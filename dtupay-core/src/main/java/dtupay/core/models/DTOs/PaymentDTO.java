package dtupay.core.models.DTOs;

import java.math.BigDecimal;
import java.util.UUID;
//@author William Embarek
public class PaymentDTO {
    public String getTokenID() {
        return tokenID;
    }

    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }

    public UUID getMerchanID() {
        return merchanID;
    }

    public void setMerchanID(UUID merchanID) {
        this.merchanID = merchanID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String tokenID;
    public UUID merchanID;
    public BigDecimal amount;
    public String description;
}
