package dtupay.core.Enums;
//@author Frederik Kirkegaard
public enum TokenStatus {
    UNUSED,
    ACCEPTED,
    REJECTED,
}