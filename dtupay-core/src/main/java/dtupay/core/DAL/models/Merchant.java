package dtupay.core.DAL.models;

import java.util.UUID;
//@author Frederik Kirkegaard
public class Merchant extends DTUPayUser {
    private Token customerToken;

    public Merchant(String cprNumber, String firstName, String lastName, UUID merchantID, String merchantAccountID) {
        super(cprNumber, firstName, lastName, merchantID, merchantAccountID);
    }

    public Token getCustomerToken() {
        return customerToken;
    }

    public void setCustomerToken(Token customerToken) {
        this.customerToken = customerToken;
    }
}
