package dtupay.core.DAL.models;

import dtupay.core.Enums.TokenStatus;

import java.util.UUID;
//@author Sebatian Fischer
public class Token {

    private String tokenID;
    private TokenStatus status;
    private UUID customerID;

    public Token(String tokenID, UUID customerID) {
        this.customerID = customerID;
        this.tokenID = tokenID;
    }

    public String getTokenID() {
        return tokenID;
    }

    public TokenStatus getStatus() {
        return status;
    }

    public void setStatus(TokenStatus status) {
        this.status = status;
    }

    public UUID getCustomerID() {
        return customerID;
    }

    public void setCustomerID(UUID customerID) {
        this.customerID = customerID;
    }
}
