package dtupay.core.DAL.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
//@author Magnus Glasdam Jakobsen
public class DTUPayUser {
    private String cprNumber;
    private String firstName;
    private String lastName;
    private UUID userID;
    private String accountID;
    private List<UUID> transactionRequestIDs;

    public DTUPayUser(String cprNumber, String firstName, String lastName, UUID userID, String accountID) {
        this.cprNumber = cprNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userID = userID;
        this.accountID = accountID;
        this.transactionRequestIDs = new ArrayList<>();
    }

    public String getCprNumber() {
        return cprNumber;
    }

    public void setCprNumber(String value) {
        this.cprNumber = value;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String value) {
        this.lastName = value;
    }

    public UUID getUserID() {
        return userID;
    }

    public void setUserID(UUID userID) {
        this.userID = userID;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public List<UUID> getTransactionRequestIDs() {
        return transactionRequestIDs;
    }

    public void setTransactionRequestIDs(List<UUID> transactionRequestIDs) {
        this.transactionRequestIDs = transactionRequestIDs;
    }
}