package dtupay.core.DAL.models;

import java.math.BigDecimal;
import java.util.UUID;
//@author Mathias Bo Jensen
public class TransactionRequest {



    public enum TransactionStatus{SUCCEEDED, PENDING, REJECTED};
    private TransactionStatus transactionStatus;
    private UUID transactionRequestID;
    private Token customerToken;
    private Merchant merchant;
    private BigDecimal amount;
    private String description;
    private Customer customer;


    public TransactionRequest(UUID transactionRequestID, Token customerToken, Merchant merchant, BigDecimal amount, Customer customer, String description) {
        this.transactionRequestID = transactionRequestID;
        this.customerToken = customerToken;
        this.merchant = merchant;
        this.amount = amount;
        this.description = description;
        this.customer = customer;
        transactionStatus = TransactionStatus.PENDING;
    }

    public UUID getTransactionRequestID() {
        return transactionRequestID;
    }

    public Token getCustomerToken() {
        return customerToken;
    }

    public void setCustomerToken(Token customerToken) {
        this.customerToken = customerToken;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchantID(Merchant merchant) {
        this.merchant = merchant;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Customer getCustomer(){
        return customer;
    }
}
