package dtupay.core.DAL.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
//@author William Embarek
public class Customer extends DTUPayUser {
    private List<Token> tokenArray;

    public Customer(String cprNumber, String firstName, String lastName, UUID customerID, String customerAccountID) {
        super(cprNumber, firstName, lastName, customerID, customerAccountID);
        this.tokenArray = new ArrayList<>();
    }

    public List<Token> getTokenArray() {
        return tokenArray;
    }

    public void setTokenArray(List<Token> tokenArray) {
        this.tokenArray = tokenArray;
    }

    public int numberOfTokens() {
        return tokenArray.size();
    }

    public Token getNextToken() {
        return tokenArray.get(0);
    }

    public void consumeToken(){
        tokenArray.remove(0);
    }

    public void addTokens(List<Token> tokenList) {
        tokenArray.addAll(tokenList);
    }
}
