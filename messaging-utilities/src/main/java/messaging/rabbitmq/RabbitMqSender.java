package messaging.rabbitmq;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import messaging.Event;
import messaging.EventSender;

public class RabbitMqSender implements EventSender {

	private static final String EXCHANGE_NAME = "eventsExchange";

	@Override
	public void sendEvent(Event event) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("rabbitMq");
		try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
			channel.exchangeDeclare(EXCHANGE_NAME, "topic");
			String message = new Gson().toJson(event);
			channel.basicPublish(EXCHANGE_NAME, "events", null, message.getBytes("UTF-8"));
		} catch (Exception e) {
			System.out.println("Exception when sending event : " + e);
		}
	}

}